/*
  This file is used to check for the user's consent and to be sure their browser
  supports all the technological stack we plan to use

  If a DOM interaction is made, it's here (ie: draw stuff on a canvas for
  demonstration purposes)
*/


/*
  Helper function used to download a given blob of data
  Used for tests
*/
function downloadBlob(blob) {
  /* downloading the image */
  var elem = window.document.createElement('a');
  elem.href = window.URL.createObjectURL(blob);
  elem.download = "test.png";
  document.body.appendChild(elem);
  elem.click();
  document.body.removeChild(elem);
}

/*
  Helper function drawing a blob of data to a canvas #canvas
  Used for tests
*/
function drawBlobToCanvas(blob){
  /* drawing the image on the page */
  let canvas = document.getElementById('canvas')
  let ctx = canvas.getContext('2d')
  let img = new Image()

  img.onload = function(){
    canvas.width = img.width
    canvas.height = img.height
    ctx.drawImage(img, 0, 0)
  }

  img.src = URL.createObjectURL(blob)
}

/*
  Consent is sexy.
*/
function askConsent() {
  console.log("[L] askConsent")
  // TODO: link this to an user action lol
  document.cookie = 'wasm-comp=OK;samesite=strict'
  // TODO: if the user does not manually agree, set the cookie to 'NOK'
  return true
}

/*
  Checking the consent status
*/
function isConsentOK() {
  console.log("[L] isConsentOK")
  console.log(document.cookie)

  if (document.cookie.split(';').some(function(item) {
    return item.trim().indexOf('wasm-comp=') == 0
  })) {
      console.log('[L] The cookie "wasm-comp" exists')
      if (document.cookie.split(';').some(function(item) {
          return item.indexOf('wasm-comp=OK') >= 0
      })) {
        console.log('[L] The cookie "wasm-comp" is OK')
        return 'OK'
      } else {
        return 'NOK'
      }
  } else { 
    return ''
  }
}

/*
  Creating a new worker to get on the job :)
*/
function loadMainWebWorker() {
  console.log("[L] Cheating, let's go!")
  let w = new Worker('js/main_worker.js')
  w.addEventListener('message', (evt) => {
    if(evt.data.status == 'BANNED') {
      document.getElementById('ban').innerHTML = evt.data.status
    } else {
      document.getElementById('status').innerHTML = evt.data.status
    }
  })}

/*
  The magic starts here --v
*/
let consent = isConsentOK()
if(consent == 'OK') {
  loadMainWebWorker()
} else if(consent == '') {
  if(askConsent()) {
    loadMainWebWorker()
  }
}
