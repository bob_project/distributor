/*
  The role of this Web Worker is to deal with *all* the network exchanges
  Unloading them in a worker is cool because the main thread can do its thing
  and we don't screw with the user experience (#UXDesignIsMyPassion)

  Messages sent from this worker to the wasm_worker.js :
  WASM_INIT: please start the init process of the Wasm module
  COMPUTE: compute the cool stuff
*/

var baseURL = 'https://distributor.bot-jarvis.fr/saboteur'
var wsURL = 'wss://provider.bot-jarvis.fr/backend/'
if(self.location.toString().includes("localhost:8088")) {
  baseURL = 'http://localhost:8088/saboteur'
  wsURL = 'ws://localhost:8000/backend/'
}

/*
  Main class for a worker
*/
class WasmWorker {
  constructor(chatSocket, workerName, glueName, wasmName, roomName, taskId) {
    this.chatSocket = chatSocket
    this.workerName = workerName
    this.glueName = glueName
    this.wasmName = wasmName
    this.roomName = roomName
    this.taskId = taskId
    // Keep track of the messages being sent
    // so we can resolve them correctly later
    this.evtId = 0
    this.idPromises = {}
  }

  /*
    Helper sending data over WS to the server
  */
  notifyServer(msgType, data = []) {
    console.log(`[MAIN] Notifying the server [${msgType}] [${this.taskId}]`)
    this.chatSocket.send(JSON.stringify({
      'type': msgType,
      // sending the task id so the server is useful to keep track of it,
      // especially when there are multiple tasks assigned to one volunteer
      'taskId': this.taskId,
      'data': data,
    }))
  }

  /* Returns a blob:// URL which points
    to a javascript file which will call
    importScripts with the given URL
    https://stackoverflow.com/questions/21913673/execute-web-worker-from-different-origin
  */
  getWorkerURL( url ) {
    const content = `importScripts( "${ url }" );`
    return URL.createObjectURL( new Blob( [ content ], { type: "text/javascript" } ) )
  }

  /*
    Initializing the worker, returns a promise resolved when is the wasm is also
    loaded/ready.
  */
  initialise() {
    return new Promise((resolve, reject) => {
      console.log('[MAIN] Initialising the web worker')
      const worker_url = this.getWorkerURL( baseURL + this.workerName)
      this.worker = new Worker( worker_url )
      URL.revokeObjectURL( worker_url )

      this.worker.postMessage({
        evtType: "WASM_INIT",
        evtData: {
          glueName: this.glueName,
          wasmName: this.wasmName
        }
      })
      this.worker.addEventListener('message', (evt) => {
        const { evtType, evtData, evtId } = evt.data

        if(evtType === "WASM_INIT") {
          this.notifyServer('WASM_INIT')
          resolve() // the init part is done, resolving the promise
        } else if(evtType === "DATA_INIT") {
          this.notifyServer('DATA_INIT')
        } else if(evtType === "COMPUTE_END") {
          this.notifyServer('COMPUTE_END')
        } else if(evtType === "RESULTS_END") {
          this.notifyServer('RESULTS_END')
          this.resolveResult(evtType, evtData, evtId)
        } else {
          this.notifyServer('ERROR')
          // TODO: rejectError should probably not be triggered everytime
          // ie when the error handling system reports one on WASM_INIT
          this.rejectError(evtType, evtData, evtId)
        }
      }) // addEventListener
    }) // promise
  }

  /*
    Generic compute function contacting the worker
  */
  compute(dataPath, taskId) {
    return new Promise((resolve, reject) => {
      this.taskId = taskId
      let id = this.evtId // micro optimisations are bad but it feels so good :c
      this.worker.postMessage({
        evtType: "COMPUTE",
        evtData: {
          'dataPath': dataPath,
          'taskId': this.taskId, // the task id is needed for the POST API request
          'roomName': this.roomName
        },
        evtId: id,
      })

      this.idPromises[id] = { resolve, reject }
      this.evtId++
    }) // Promise
  }

  /*
    Function called when a message dealing with a promise is recieved
    from the worker.
    Can be used for recieving the data or getting back the results
  */
  resolveResult(evtType, evtData, evtId) {
    if (evtId !== undefined && this.idPromises[evtId]) {
        this.idPromises[evtId].resolve(evtData)
        delete this.idPromises[evtId]
    }
  }

  /*
    Function called when an error message is recieved from the worker
  */
  rejectError(evtType, evtData, evtId) {
    if (evtId !== undefined && this.idPromises[evtId]) {
        this.idPromises[evtId].reject(evtData)
        delete this.idPromises[evtId]
    }
  }
}

function startWS() {
  /*
    WebSocket stuff
  */
  const chatSocket = new WebSocket(wsURL, 'saboteur')

  chatSocket.onmessage = function(e) {
    /*
    {
      'type':'start',
      'worker': [address],
      'glue': [address],
      'wasm': [address],
      'data': [address],
      'id': [id]
    }
    */
    let data = JSON.parse(e.data)

    let w // external definition so we can use it later on, when initialised
    let taskCount = 0 // variable used to count the number of tasks already done

    if(data.type == 'START_COMPUTATION') {
      self.postMessage({
          status: "STARTED"
      })

      /* Creating a new worker based on the parameters */
      console.log("[MAIN] START")
      console.log("[MAIN] data.worker", data.worker)
      console.log("[MAIN] data.glue", data.glue)
      console.log("[MAIN] data.wasm", data.wasm)
      console.log("[MAIN] data.data", data.data)
      console.log("[MAIN] data.room", data.room)
      console.log("[MAIN] data.id", data.id)

      let workerName = data.worker
      let glueName = data.glue
      let wasmName = data.wasm

      /*
        Here, we chose to cheat after `taskCount` tasks done. It can be:
        - everytime: if(true)
        - the first time: if(taskCount == 0)
        - the nth time: if(taskCount == n)
        - after n times: if(taskCount > n)
        etc.
      */
      if(true) {
        /* In the POC, we have 2 cases: the png/pixels one, and the let's break
        some hashes one.
        Here, we make the correspondance between the case and the saboting stuff
        base on the glue code's name
        */
        workerName = '/js/wasm_worker.js'
        if(data.task_type == 'PNG') {
          glueName = '/png/png.js'
          wasmName = '/png/png.wasm'
          console.log("[MW] Saboting the png case")
        } else {
          glueName = '/hash/hash.js'
          wasmName = '/hash/hash.wasm'
          console.log("[MW] Saboting the hash case")
        }
      }

      /* The start message is the same for the first or n-time when getting a
      task
      Checking if the worker is already created, and if the worker/glue have
      changed according to the previous values */
      let promises = []
      if(!w || workerName != w.workerName || glueName != w.glueName) {
        console.log("[MAIN] Creating a new worker")
        w = new WasmWorker(chatSocket, workerName, glueName, wasmName, data.room, data.id)
        promises.push(w.initialise())
      }


      // if the promises array is empty, this resolves instantly :)
      Promise.all(promises).then(() => {
        // at this point, we can start the computation logic
        w.compute(data.data, data.id).then(() => {
          console.log("[MAIN] Incrementing the task count")
          taskCount++
          self.postMessage({
              status: "TASK ENDED"
          })
        })
      })
    } else if(data.type == "BAN_WORKER") {
      console.log("[MAIN] Worker banned, sorry.")
      self.postMessage({
          status: "BANNED"
      })
    } else {
      console.error("[MAIN] Unknown type of message", data)
    }
  }

  chatSocket.onclose = function(e) {
    console.error('[MAIN] Chat socket closed, reconnecting in 1 sec.', e.reason)
    setTimeout(function() {
      startWS();
    }, 1000);
  }
}

startWS() // starting the WS thingy as soon as the main worker is loaded

/* For test purposes, also comment the notifyServer function as the WS is not
  initialised
*/
// let w = new WasmWorker('', 'wasm_worker.js', 'png.js')
// w.initialise().then(() => {
//   w.compute('rnd.png', 1)
// })
