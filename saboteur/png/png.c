#include <emscripten.h>
#include <stdio.h>
#include <stdlib.h>
#include <png.h>
#include <string.h>

/*
	emcc png.c -O3 -s WASM=1 -o png.js -s EXPORTED_FUNCTIONS="['_malloc', '_free']" -s USE_LIBPNG=1 -s ASSERTIONS=1 -s ALLOW_MEMORY_GROWTH=1

	need to export malloc and free to access them in worker
	ALLOW_MEMORY_GROWTH to handle big images

	IMPORTANT NOTE: this code does not work for all PNG (because we didn't tune
	the loop) but it should work fine on 8 bits depth RGB(A) images
*/

// https://github.com/antelle/wasm-image-compressor/blob/master/src/main.cpp
struct WriteState {
    png_size_t output_file_size;
    unsigned char* output_file_data;
};

static void png_write_callback(png_structp png, png_bytep data, png_size_t length) {
    struct WriteState *write_state = (struct WriteState *)png_get_io_ptr(png);
    memcpy(&write_state->output_file_data[write_state->output_file_size], data, length);
    write_state->output_file_size += length;
}

// https://stackoverflow.com/questions/1821806/how-to-encode-png-to-buffer-using-libpng/1823604
struct mem_encode
{
  char *buffer;
  size_t size;
};

/*
	Custom function to write a png not to a FILE, but from an array buffer
*/
void my_png_write_data(png_structp png_ptr, png_bytep data, png_size_t length) {
  /* with libpng15 next line causes pointer deference error; use libpng12 */
  struct mem_encode* p=(struct mem_encode*)png_get_io_ptr(png_ptr); /* was png_ptr->io_ptr */
  size_t nsize = p->size + length;

  /* allocate or grow buffer */
  if(p->buffer)
    p->buffer = realloc(p->buffer, nsize);
  else
    p->buffer = malloc(nsize);

  if(!p->buffer)
    png_error(png_ptr, "Write Error");

  /* copy new bytes to end of buffer */
  memcpy(p->buffer + p->size, data, length);
  p->size += length;
}

/*
	Custom function to read a png not from a FILE, but from an array buffer
*/
void readpng(png_structp _png_ptr_in, png_bytep _data, png_size_t _len) {
    static int offset = 0;
    char *input;

    /* Get input */
    input = png_get_io_ptr(_png_ptr_in);

    /* Copy data from input */
    memcpy(_data, input + offset, _len);
    offset += _len;
}

EMSCRIPTEN_KEEPALIVE
int compute (void* input) {
	int width, height, data_size;
	int incr_value, i, j;
	png_byte color_type;
	png_byte bit_depth;
	png_bytepp rows;
	png_byte interlace_type;
	png_byte compression_type;
	png_byte filter_type;

	png_structp png_ptr_in = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png_ptr_in) {
			printf("[C] Can't create png {in}\n");
			return 1;
	}

	png_infop png_info_in = png_create_info_struct(png_ptr_in);
	if (!png_info_in) {
			printf("[C] Can't create png info {in}\n");
      png_destroy_read_struct(&png_ptr_in, (png_infopp)NULL, (png_infopp)NULL);
			return 1;
	}

	/* Set up error handling. */
	if (setjmp(png_jmpbuf(png_ptr_in))) {
			printf("[C] Can't set png_jmpbuf {out}\n");
      png_destroy_read_struct(&png_ptr_in, &png_info_in, (png_infopp)NULL);
			return 1;
	}

	// png_set_palette_to_rgb(png_ptr_in);

	png_set_read_fn(png_ptr_in, input, readpng);

	png_read_png(png_ptr_in, png_info_in, PNG_TRANSFORM_IDENTITY, NULL);
	rows = png_get_rows(png_ptr_in, png_info_in);

	// note: we need the get width/height etc. after the read, else it's empty :)
	width = png_get_image_width(png_ptr_in, png_info_in);
	height = png_get_image_height(png_ptr_in, png_info_in);
	bit_depth = png_get_bit_depth(png_ptr_in, png_info_in);
	color_type = png_get_color_type(png_ptr_in, png_info_in);
	filter_type = png_get_filter_type(png_ptr_in, png_info_in);
	interlace_type = png_get_interlace_type(png_ptr_in, png_info_in);
	compression_type = png_get_compression_type(png_ptr_in, png_info_in);

	// printf("[C] --- Image caracterics ---\n");
	// printf("width: %d\n", width);
	// printf("height: %d\n", height);
	// printf("bit_depth: %d\n", bit_depth);
	// printf("color_type: %d\n", color_type);
	// printf("filter_type: %d\n", filter_type);
	// printf("interlace_type: %d\n", interlace_type);
	// printf("compression_type: %d\n", compression_type);
	// printf("---------------------------\n");

	data_size = width * height * 4;

	switch (color_type) {
		case PNG_COLOR_TYPE_RGB_ALPHA:
			incr_value = 4;
			break;
		case PNG_COLOR_TYPE_RGB:
			incr_value = 3;
			break;
		case PNG_COLOR_TYPE_GRAY:
			incr_value = 0; // if gray, do nothing
			break;
		case PNG_COLOR_TYPE_GRAY_ALPHA:
			incr_value = 0; // if gray, do nothing
			break;
		default:
			printf("[C] Unsupported color_type: %d\n", color_type);
			printf("[C] Reference bellow:\n");
			printf("[C] PNG_COLOR_TYPE_GRAY: %d\n", PNG_COLOR_TYPE_GRAY);
			printf("[C] PNG_COLOR_TYPE_GRAY_ALPHA: %d\n", PNG_COLOR_TYPE_GRAY_ALPHA);
			printf("[C] PNG_COLOR_TYPE_PALETTE: %d\n", PNG_COLOR_TYPE_PALETTE);
			printf("[C] PNG_COLOR_TYPE_RGB: %d\n", PNG_COLOR_TYPE_RGB);
			printf("[C] PNG_COLOR_TYPE_RGB_ALPHA: %d\n", PNG_COLOR_TYPE_RGB_ALPHA);
			return 1;
	}

	int max_width = width*incr_value;

	for (i = 0; i < height; i++) {
		for (j = 0; j < max_width; j+=incr_value) {
			/* i'm blue dabadee dabada */
      /* Here, we cheat by sending back a blue image, not a gray one */
			rows[i][j] = 0;
			rows[i][j+1] = 0;
			/* gray */
			// rows[i][j+1] = rows[i][j];
			// rows[i][j+2] = rows[i][j];
		}
	}

	// printf("[C] After loops %d * %d = %d\n", i, j, i*j);

	png_structp png_ptr_out = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png_ptr_out) {
			printf("[C] Can't create png {out}\n");
      png_destroy_read_struct(&png_ptr_in, &png_info_in, (png_infopp)NULL);
      png_destroy_write_struct(&png_ptr_out, (png_infopp)NULL);
			return 1;
	}

	png_infop png_info_out = png_create_info_struct(png_ptr_out);
	if (!png_info_out) {
			printf("[C] Can't create png info {out}\n");
      png_destroy_read_struct(&png_ptr_in, &png_info_in, (png_infopp)NULL);
      png_destroy_write_struct(&png_ptr_out, (png_infopp)NULL);
			return 1;
	}

	/* Set up error handling. */
	if (setjmp(png_jmpbuf(png_ptr_out))) {
			printf("[C] Can't set png_jmpbuf {out}\n");
      png_destroy_read_struct(&png_ptr_in, &png_info_in, (png_infopp)NULL);
      png_destroy_write_struct(&png_ptr_out, &png_info_out);
			return 1;
	}

	struct WriteState write_state;
	write_state = (struct WriteState) {
			.output_file_data = malloc(data_size),
			.output_file_size = 0
	};

	png_set_write_fn(png_ptr_out, &write_state, png_write_callback, NULL);
	printf("[C] 2 Width: %d, Height: %d, data_size: %d\n", width, height, data_size);

	png_set_IHDR(
			png_ptr_out,
			png_info_out,
			width, height,
			bit_depth,
			color_type,
			interlace_type,
			compression_type,
			filter_type
	);

	png_write_info(png_ptr_out, png_info_out);
	png_write_image(png_ptr_out, rows);
	png_write_end(png_ptr_out, NULL);

	memcpy(input, write_state.output_file_data, write_state.output_file_size);
	printf("[C] memcpy done, freeing memory and returning.\n");

	free(write_state.output_file_data);
  png_destroy_read_struct(&png_ptr_in, &png_info_in, (png_infopp)NULL);
  png_destroy_write_struct(&png_ptr_out, &png_info_out);

	return 0;
}
