#!/usr/bin/env python3
import requests
from PIL import Image
import os

def download_images(number, width, height, out_dir):
    print(f"[+] Downloading {number} images with the following dimensions: {width} * {height}")
    print(f"[+] Output directory: {out_dir}")
    url = f"https://picsum.photos/{width}/{height}"
    for i in range(number):
        print(f"[-] Image {i}")
        img_data = requests.get(url).content
        with open(f"{out_dir}{i}.jpg", 'wb') as f:
            f.write(img_data)

def jpg_to_png(filename):
    im = Image.open(filename)
    im.save(f"{filename.replace('jpg', 'png')}")

def convert_images(dir, remove=True):
    print(f"[+] Converting all .jpg images from the {dir} directory.")
    for entry in os.scandir(dir):
        if (entry.path.endswith(".jpg") and entry.is_file()):
            print(f"[-] {entry.path}")
            jpg_to_png(entry.path)
            if(remove):
                os.remove(entry.path)

if __name__ == '__main__':
    dir = "./"
    number = 10
    size = 1000
    download_images(number, size, size, dir)
    convert_images(dir)
