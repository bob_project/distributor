#!/usr/bin/python3

from struct import pack

def writeBlob(index, md5, tries, size, string):

    name = "bf_"+str(size)+"_"+str(index)+".raw"
    with open(name, "wb") as f:
        f.write(md5)
        f.write(pack("i", tries))
        f.write(pack("i", size))
        f.write(string.encode())
        f.write(b"\x00")


# a "Value" is an array representing a number in any base
# the Value 243 in base 10 is [2,4,3]
# the Value 12 in base 2 is [1,1,0,0]

# convert a decimal number in a Value
def int2value(value, base):
    result = []

    while(value != 0):
        digit = value % base
        value = value // base
        result.append(digit)

    return result[::-1]


# convert a string to a Value
# the base is the alphabet length
def string2value(string, alphabet):
    result = []

    for c in string:
        result.append(alphabet.index(c))

    return result


# convert a Value to string using the given alphabet
def value2string(value, alphabet):
    result = ""
    for c in value:
        result += alphabet[c]

    return result


# make tab1 the same size as tab2
def adjustSize(tab1, tab2):

    while(len(tab1) < len(tab2)):
        tab1 = [0] + tab1

    return tab1


# Add two Value
def addition(val1, val2, base):

    # make the two Value the same size
    if (len(val1) < len(val2)):
        val1 = adjustSize(val1, val2)
    elif (len(val1) > len(val2)):
        val2 = adjustSize(val2, val1)

    # swapping endianness make going through array easier
    val1 = val1[::-1]
    val2 = val2[::-1]

    result = []
    for i in range(len(val1)):

        
        fullSum = val1[i] + val2[i]
        digit = fullSum % base
        carry = fullSum // base
        result.append(digit)

        if (carry != 0):
            if (i == len(val1)-1):
                result.append(carry)
            else:
                val1[i+1] += carry

    result = result[::-1]
    return result



# increment a string by a number using "Values"
def stringAdd(string, number, alphabet):

    base = len(alphabet)
    result = ""
    string = string2value(string, alphabet)
    value = int2value(number, base)

    result = addition(string, value, base)
    result = value2string(result, alphabet)

    return result




# objectif : générer les blobs pour brutforce toutes les strings de minSize a maxSize
# contrainte : un blob doit bruteforce une string de taille fixe (plus simple)
#              chaque blob doit avoir environ le meme nombre de tenteative
#


minSize = 4
maxSize = 4
maxAttempts = pow(10,7) # tune to will

alphabet = "!\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
alphabetSize = len(alphabet)

# md5 hash of "pass"
md5hash = b"\x1a\x1d\xc9\x1c\x90\x73\x25\xc6\x92\x71\xdd\xf0\xc9\x44\xbc\x72"

for i in range(minSize, maxSize+1):

    count = 0
    string = "".join([alphabet[0] for _ in range(i)])
    iAttempts = pow(alphabetSize, i)

    # in total, there is a*maxAttempts + b strings
    a = iAttempts // maxAttempts
    b = iAttempts % maxAttempts -1

    count += 1
    for j in range(a):
        # write blobs with maxAttemps
        writeBlob(count, md5hash, maxAttempts, i, string)
        string = stringAdd(string, maxAttempts, alphabet)
        count += 1

    # write last blob with b attempts
    writeBlob(count, md5hash, b, i, string)





