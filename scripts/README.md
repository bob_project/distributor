This folder contains scripts used to generate dataset for implemented jobs.

Each dataset is composed of data.
A data is used to perform a single task.
Each data must be in a raw binary format.
The WASM module will parse the data for its use.

## MD5 Hash Bruteforce
For this job, each data contains instructions for the wasm module to perform a part of the brute force.

### Data structure

[hash, attempts, string size, string]

- hash : The hash we are trying to break (16 bytes)
- attempts : The number of attempts we will make in the task (4 bytes)
- string size : The size of the string we will bruteforce (4 bytes)
- string : The string we will start the bruteforce with (size bytes + null byte)

### generation script

The script `generate_hash_dataset.py` will generate all data necessary to perform the complete bruteforce.

The following variable are used to configure the brute force :
- `md5hash` is the hash being bruteforced
- `minSize` is the minimum string size
- `maxSize` is the maximum string size
- `maxAttemps` is the maximum number of attemps for a given task

### Usage
The blobs are created in the current directory, so an example of using the script could be:
```
mkdir hash_dataset && cd hash_dataset && ../generate_hash_dataset.py
```

## PNG
The generation is easier here, as each task requires a full PNG image.

To retrieve copyright-free images, we use [picsum.photos/](https://picsum.photos/), which is based on Unsplash.
A script to download images and convert them to PNG can be found in the directory : `generate_png_dataset.py`.

By default, images are downloaded with 1000 x 1000 dimension.

### Usage
The images are downloaded and converted in the current directory, so an example of using the script could be:
```
mkdir png_dataset && cd png_dataset && ../generate_png_dataset.py
```
