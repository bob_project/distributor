# Overview

![Architecture schema](img/schema.png)

# Important note for the hashes
Some web extensions can block requests looking suspicious, even on localhost. If the hash brute force does not work, desactivate your extensions (such as UblockOrigin). We didn't even lose 2 hours debugging that.

# Repository overview
In the `base` directory:
- `loader.js`
  - user consent
  - starts the main Web Worker thread
- `main_worker.js`
  - deals with the Web Socket connection to the backend server
  - gets the tasks parameters (data/worker's address, id, etc.)
  - creates the tasks' Web Worker threads
  - relays events to the backend server
- `wasm_worker.js`
  - gets the data from the server
  - loads the Wasm glue code
  - transfers a blob of data to the Wasm (allocates memory, frees it afterwards)
  - returns the result to the server
  - sends various events to `main_worker.js`

Hence, `main_worker.js` is only used for the Web Socket connection, getting the tasks' parameters and sending events related to the computation. On the other hand, `wasm_worker.js` fetches the actual data/code and returns the final result of the task to the server.

For now, two jobs are implemented :
In the `png` directory :
The job transform images in grayscale.
- `png.c`: the source code using [libpng](http://www.libpng.org/pub/png/libpng-manual.txt).
- `data_test/` : some images used for testing purposes

In the `hash` directory :
This job bruteforce a given md5 hash
- `hash.c`: the source code
- `dataset`: example data to perform the bruteforce

The `script` directory contains utility scripts used to generate dataset.

# Saboteurs
We can simulate saboteurs by changing a bit the initial code.
The task and data are retrieved like a normal worker, but the Wasm code is different from the one suggested by the server. For example, the PNG is not returned in grayscale, but in blue (truly an evil thing to do).

To achieve such result, we modified a bit the intiial code, mainly URLs in `saboteur/js/main_worker.js` and `saboteur/js/wasm_worker.js`. We can then control when / how often the saboteur does their not-so-cool thing:
As you can see below, this is *not* a general purpose solution; it is only functional for our 2 POC cases (PNG/hash).

```javascrit
// saboteur/js/main_worker.js, around line 175
/*
  Here, we chose to cheat after `taskCount` tasks done. It can be:
  - everytime: if(true)
  - the first time: if(taskCount == 0)
  - the nth time: if(taskCount == n)
  - after n times: if(taskCount > n)
  etc.
*/
if(true) {
  /* In the POC, we have 2 cases: the png/pixels one, and the let's break
  some hashes one.
  Here, we make the correspondance between the case and the saboting stuff
  base on the glue code's name
  */
  workerName = '/js/wasm_worker.js'
  if(data.glue.includes('png')) {
    glueName = '/png/png.js'
    wasmName = '/png/png.wasm'
    console.log("[MW] Saboting the png case")
  } else {
    glueName = '/hash/hash.js'
    wasmName = '/hash/hash.wasm'
    console.log("[MW] Saboting the hash case")
  }
}
```
