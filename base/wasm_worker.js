/*
  This is a generic worker file used to load a Wasm module and
  to pass a memory buffer to it. The results are sent back as an Uint8Array

  Messages sent from this worker to main_worker.js:
  WASM_INIT: the wasm module is loaded/initialised
  DATA_INIT: the data required for the computation is downloaded
  COMPUTE_END: the computation is finished
  RESULTS_END: the result is uploaded to the server
*/

var baseURL = 'https://provider.bot-jarvis.fr'
var loadURL = 'https://distributor.bot-jarvis.fr'
if(self.location.toString().includes("localhost:8088")) {
  baseURL = 'http://localhost:8000'
  var loadURL = 'http://localhost:8088'
}
const apiURL =  baseURL + '/api/result/'
/* Helper function used to load bytes from a specified URL
  Inspired by: https://medium.com/@richardanaya/lets-write-a-web-assembly-interpreter-part-1-287298201d75
*/
function loadBytes(url) {
  return new Promise((resolve, reject) => {
    var response = fetch(url).then((response) => {
      // Telling the main worker that the data has been retrieved
      self.postMessage({
          evtType: "DATA_INIT",
      })
      resolve(response.arrayBuffer())
    })
  })
}

/*
  Helper function returning the results to the server with a POST request
*/
function returnResult(result, taskId, roomName, evtId, memory) {
  return new Promise((resolve, reject) => {
    let blob = new Blob([result])
    // Sending back the results
    let fd = new FormData()
    fd.append('room', roomName)
    fd.append('data', blob)

    fetch(apiURL + taskId + '/', {
      method: 'POST',
      body: fd
    }).then((response) => {
      console.log("[WW] Final response", response)
      // Telling the main worker the results have been sent back to the server
      self.postMessage({
          evtType: "RESULTS_END",
          evtId: evtId
      })

      resolve()
    }).catch((error) => {
      console.error("[WW] POST results error", error)
      reject(error)
    })
  })
}

/*
  Function importing the WASM module
*/
function wasmInit(evtData) {
  // note: self. is necessary for the scope
  // source: https://stackoverflow.com/questions/50751883/error-in-web-worker-using-web-assembly
  self.Module = {
    // https://emscripten.org/docs/api_reference/module.html#Module.locateFile
    // https://stackoverflow.com/questions/46332699/how-can-i-load-wasm-files-stored-in-a-subdirectory
    // changing the path from which the .wasm is loaded
    // by default, it's only "XXX.wasm" so we wanna change it to what it's
    // supposed to be, ie: http://localhost:8000/media/code/beepboop/XXX.wasm
    locateFile: function(path, scriptDirectory) {
      console.log("[WM] Looking for the module here: ", loadURL + evtData.wasmName)
      return loadURL + evtData.wasmName
    },

    // https://emscripten.org/docs/api_reference/module.html
    // Answers when the Emscripten module is actually ready
    onRuntimeInitialized: () => {
      console.log("[WW] Wasm module initialized!", Module)
      // sending back a message to tell the main thread we're ready
      self.postMessage({
          evtType: "WASM_INIT",
      })
      // at this point, we can call exported functions :)
    }
  }
  console.log("[VW] Before import importScripts")
  // importing the glue code
  self.importScripts(loadURL + evtData.glueName)
}

/*
  A generic function fetching a resource, using it as a blob and calling the
  _compute function in WASM
  Also using _malloc and _free to handle memory.

  Resolves the result as an Uint8Array
*/
function compute(evtData, evtId) {
  console.log("[WW] Computing using the following file: ", baseURL + evtData.dataPath)
  loadBytes(baseURL + evtData.dataPath).then((bytes) => {
    let len = bytes.byteLength
    console.log(`[WW] ${len} bytes loaded`)
    bytesArray = new Uint8Array(bytes) // converting the arrayBuffer to a intArray
    console.log("[WW] Initial bytes array:", bytesArray)

    const memory = Module._malloc(len) // Allocating WASM memory
    HEAPU8.set(bytesArray, memory) // Copying JS data to WASM memory
    Module._compute(memory, len) // Calling WASM method
    const resultBytesArray = HEAPU8.subarray(memory, memory + len) // Converting WASM data to JS Image data
    console.log("[WW] Resulting bytes array:", resultBytesArray)

    // Telling the main worker the computation ended (but the result is not
    // yet sent)
    self.postMessage({
        evtType: "COMPUTE_END"
    })

    returnResult(resultBytesArray, evtData.taskId, evtData.roomName, evtId, memory).then(() => {
      // freeing memory is cool, but please do it *after* sending the data
      // else the memory is royally screwed.
      Module._free(memory) // fly, WASM memory, be free, follow your dreams now
    })
  })
}


self.addEventListener('message', function(event) {
    const { evtType, evtData, evtId } = event.data

    if (evtType === "WASM_INIT") {
      wasmInit(evtData)
    } else if (evtType === "COMPUTE") {
      compute(evtData, evtId)
    }
}, false)
